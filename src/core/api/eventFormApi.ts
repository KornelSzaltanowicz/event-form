import { ApiRequestException } from '../exceptions/ApiErrorException';
import { ICategoryModel } from '../redux/category/types';
import { IResponsibleData } from '../redux/responsible/types';

import axios from 'axios';

export interface IEventFormApi {
  getCategories: () => Promise<ICategoryModel[]>;
  getResponsibles: () => Promise<IResponsibleData[]>;
}

class EventFormApi implements IEventFormApi {
  private readonly _apiUrl: string;

  constructor(baseUrl: string) {
    this._apiUrl = baseUrl;
  }

  getCategories = async (): Promise<ICategoryModel[]> => {
    try {
      const { data } = await axios.get(this._apiUrl + '5bcdd3942f00002c00c855ba');
      return data;
    } catch (error) {
      throw new ApiRequestException();
    }
  };

  getResponsibles = async (): Promise<IResponsibleData[]> => {
    try {
      const { data } = await axios.get(this._apiUrl + '5bcdd7992f00006300c855d5');
      return data;
    } catch (error) {
      throw new ApiRequestException();
    }
  };
}

export default new EventFormApi('http://www.mocky.io/v2/');

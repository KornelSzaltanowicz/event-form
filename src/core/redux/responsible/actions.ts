import { createAsyncThunk } from '@reduxjs/toolkit';
import eventFormApi from '../../api/eventFormApi';
import { IResponsibleData } from './types';

export const fetchResponsibles = createAsyncThunk('fetchResponsibles', async () => {
  try {
    const data = await eventFormApi.getResponsibles();
    return data.map((item: IResponsibleData) => ({
      ...item,
      label: item.name + ' ' + item.lastname,
      value: item.id,
    }));
  } catch (error) {}
});

import { createSlice } from '@reduxjs/toolkit';
import { IMainReduxState, LoadedStateEnum, TaskStateEnum } from '../types';
import { IResponsibleData } from './types';
import { fetchResponsibles } from './actions';

const initialState: IMainReduxState<IResponsibleData> = {
  status: TaskStateEnum.IDLE,
  loaded: LoadedStateEnum.UNLOADED,
  error: undefined,
  data: null,
};

const responsibleSlice = createSlice({
  name: 'responsible',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchResponsibles.fulfilled.type]: (state, action) => ({
      ...state,
      data: action.payload,
      loaded: LoadedStateEnum.LOADED,
      status: TaskStateEnum.DONE,
    }),
    [fetchResponsibles.rejected.type]: (state, action) => ({
      ...state,
      error: action.payload,
      status: TaskStateEnum.ERROR,
    }),
    [fetchResponsibles.pending.type]: (state) => ({
      ...state,
      status: TaskStateEnum.PENDING,
    }),
  },
});

export default responsibleSlice;

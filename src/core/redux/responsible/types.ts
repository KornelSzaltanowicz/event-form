import { ISelectModel } from '../types';

export interface IResponsibleData extends ISelectModel {
  id: number;
  name: string;
  lastname: string;
  email: string;
}

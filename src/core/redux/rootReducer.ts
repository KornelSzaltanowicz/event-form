import { combineReducers } from '@reduxjs/toolkit';
import categorieslice from './category/slice';
import responsiblesSlice from './responsible/slice';

const rootReducer = combineReducers({
  categories: categorieslice.reducer,
  responsibles: responsiblesSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;

import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";

function createReduxStore() {
  const middlewares = [
    ...getDefaultMiddleware({
      serializableCheck: true,
    }),
  ];

  return configureStore({
    reducer: rootReducer,
    middleware: middlewares,
    devTools: process.env.NODE_ENV !== "production",
  });
}

export const store = createReduxStore();

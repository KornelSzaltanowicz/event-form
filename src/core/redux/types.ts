export enum LoadedStateEnum {
  LOADED = 'LOADED',
  UNLOADED = 'UNLOADED',
}

export enum TaskStateEnum {
  IDLE = 'IDLE',
  PENDING = 'PENDING',
  DONE = 'DONE',
  ERROR = 'ERROR',
}

export interface IMainReduxState<T> {
  status: TaskStateEnum;
  loaded: LoadedStateEnum;
  error: unknown;
  data: Array<T> | null;
}

export interface ISelectModel {
  label: string;
  value: string | number;
}

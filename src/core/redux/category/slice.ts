import { createSlice } from '@reduxjs/toolkit';
import { IMainReduxState, LoadedStateEnum, TaskStateEnum } from '../types';
import { ICategoryModel } from './types';
import { fetchCategoriess } from './actions';

const initialState: IMainReduxState<ICategoryModel> = {
  status: TaskStateEnum.IDLE,
  loaded: LoadedStateEnum.UNLOADED,
  error: undefined,
  data: null,
};

const coordinatorSlice = createSlice({
  name: 'coordinator',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchCategoriess.fulfilled.type]: (state, action) => ({
      ...state,
      data: action.payload,
      loaded: LoadedStateEnum.LOADED,
      status: TaskStateEnum.DONE,
    }),
    [fetchCategoriess.rejected.type]: (state, action) => ({
      ...state,
      error: action.payload,
      status: TaskStateEnum.ERROR,
    }),
    [fetchCategoriess.pending.type]: (state) => ({
      ...state,
      status: TaskStateEnum.PENDING,
    }),
  },
});

export default coordinatorSlice;

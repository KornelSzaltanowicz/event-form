import { ISelectModel } from '../types';

export interface ICategoryModel extends ISelectModel {
  id: number;
  name: string;
}

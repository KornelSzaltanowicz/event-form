import { createAsyncThunk } from '@reduxjs/toolkit';
import eventFormApi from '../../api/eventFormApi';
import { ICategoryModel } from './types';

export const fetchCategoriess = createAsyncThunk('fetchCategoriess', async () => {
  try {
    const data = await eventFormApi.getCategories();
    return data.map((item: ICategoryModel) => ({
      ...item,
      label: item.name,
      value: item.id,
    }));
  } catch (error) {}
});

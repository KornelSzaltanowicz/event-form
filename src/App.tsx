import React from 'react';
import Container from './components/Container/Container';
import Header from './components/Header/Header';
import EventForm from './components/EventForm';

function App() {
  return (
    <main>
      <Header title="New event" />
      <Container>
        <EventForm />
      </Container>
    </main>
  );
}

export default App;

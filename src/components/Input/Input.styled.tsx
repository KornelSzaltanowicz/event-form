import styled from 'styled-components';

export const InputStyled = styled.input<{ hasError?: boolean }>`
  width: 100%;
  font-size: 14px;
  padding: 10px;
  min-height: 26px;
  outline: none;
  box-sizing: border-box;
  color: #9e9e9e;
  border: 1px solid ${({ hasError }) => (hasError ? '#ef5350' : '#e0e0e0')};

  &::placeholder {
    color: #9e9e9e;
    opacity: 1;
  }
`;

export const InputWrapper = styled.div`
  width: 100%;
  position: relative;
`;

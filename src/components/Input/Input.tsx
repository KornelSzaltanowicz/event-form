import * as React from 'react';
import ErrorBadge from '../ErrorBadge/ErrorBadge';
import { InputStyled, InputWrapper } from './Input.styled';

export interface IInput extends React.ComponentPropsWithoutRef<'input'> {
  error?: string;
  hasError?: boolean;
}

function Input(props: IInput) {
  const { error, hasError = false, className, ...rest } = props;
  return (
    <InputWrapper className={className}>
      <InputStyled {...rest} hasError={hasError} />
      {hasError && <ErrorBadge message={error} />}
    </InputWrapper>
  );
}

export default Input;

import * as React from 'react';
import { FormFieldStyled, Label, RequiredStar } from './FormField.styled';

export interface IFormField {
  label: string;
  required?: boolean;
  htmlFor?: string;
  children?: React.ReactNode;
  centered?: boolean;
  inline?: boolean;
  hasError?: boolean;
}

function FormField(props: IFormField) {
  const { required = false, label, children, htmlFor, centered, hasError, ...rest } = props;
  return (
    <FormFieldStyled hasError={hasError} {...rest}>
      <Label htmlFor={htmlFor} centered={centered} hasError={hasError}>
        {label} {required && <RequiredStar>*</RequiredStar>}
      </Label>
      {children}
    </FormFieldStyled>
  );
}

export default FormField;

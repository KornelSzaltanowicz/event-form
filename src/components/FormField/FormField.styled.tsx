import styled, { css } from 'styled-components';

export const FormFieldStyled = styled.div<{ inline?: boolean; hasError?: boolean }>`
  position: relative;
  display: flex;
  width: 100%;
  flex-direction: column;
  padding: ${({ hasError }) => (hasError ? '14px 0' : '8px 0')};
  transition: all 0.2s;

  @media (min-width: 576px) {
    flex-direction: row;
    transition: all 0.2s;
    padding: 14px 0;
  }

  @media (min-width: 992px) {
    width: 60%;
  }
`;

export const Label = styled.label<{ centered?: boolean; hasError?: boolean }>`
  display: inline-block;
  padding: ${({ centered }) => (!centered ? '10px 0' : '0')};
  ${({ centered }) =>
    centered &&
    css`
      align-self: flex-start;

      @media (min-width: 576px) {
        align-self: center;
      }
    `}
  min-width: 120px;
  font-size: 14px;
  font-weight: 600;
  color: ${({ hasError }) => (hasError ? '#ef5350' : '#e0e0e0')};
  text-transform: uppercase;
  letter-spacing: -1px;
`;

export const RequiredStar = styled.span`
  color: #ef5350;
`;

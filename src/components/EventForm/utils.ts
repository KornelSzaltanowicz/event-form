import { format } from 'date-fns';
import { IEventFormValues, ISubmitValues } from './types';

export function mapFormValues(values: IEventFormValues): ISubmitValues {
  const [hours, minutes] = values.time.split(':');
  const hoursNumber = Number(hours);
  const minutesNumber = Number(minutes);
  const date = new Date(values.date);
  const fullYear = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDay();

  const formattedDate = format(new Date(fullYear, month, day, hoursNumber, minutesNumber), "yyyy-MM-dd'T'HH:mm");

  const result: ISubmitValues = {
    title: values.title,
    description: values.description,
    category_id: Number(values?.category?.value),
    paid_event: values.payment === 'paid_event',
    // dunno if event_fee is required if paid_event is set to false
    event_fee: values.payment === 'paid_event' ? Number(values.fee) || 0 : 0,
    reward: values.reward,
    date: formattedDate, // YYYY-MM-DDTHH:mm (example: 2018-01-19T15:15)
    duration: Number(values.duration) * 60 * 60, // in seconds
    coordinator: {
      email: values.email,
      id: values?.coordinator?.value as string,
    },
  };

  return result;
}

export const emailRegexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

import { IRadioOption } from '../RadioGroup/RadioGroup';
import { ISelectOption } from '../Select/Select';

export const selectOptions: ISelectOption[] = [
  { label: 'Category 1 ', value: 1 },
  { label: 'Category 2 ', value: 2 },
  { label: 'Category 3 ', value: 3 },
];

export const paymentOptions: IRadioOption[] = [
  { label: 'Free event', value: 'free_event' },
  { label: 'Paid event', value: 'paid_event' },
];

export const dayTimeOptions: IRadioOption[] = [
  { label: 'AM', value: 'am' },
  { label: 'PM', value: 'pm' },
];

export interface IEventFormValues {
  title: string;
  description: string;
  category: ISelectOption | null;
  payment: string;
  reward: number;
  coordinator: ISelectOption | null;
  email: string;
  date: string;
  time: string;
  daytime: string;
  duration: number;
  fee?: string;
  [key: string]: any;
}

export interface ISubmitValues
  extends Pick<IEventFormValues, 'title' | 'description' | 'reward' | 'date' | 'duration'> {
  category_id: number;
  paid_event: boolean;
  event_fee: number | string;
  coordinator: {
    email: string;
    id: string;
  };
}

export const requiredField = ['title', 'description', 'date', 'coordinator'];

export const initialFormValues: IEventFormValues = {
  title: '',
  category: null,
  description: '',
  payment: paymentOptions[0].value,
  reward: 0,
  coordinator: null,
  email: '',
  date: '',
  duration: 0,
  time: '12:00',
  daytime: dayTimeOptions[0].value,
  fee: '',
};

export interface IErrorState extends Partial<Record<keyof IEventFormValues, string>> {}

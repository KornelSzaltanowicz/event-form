import styled from 'styled-components';
import Input from '../Input/Input';
import RadioGroup from '../RadioGroup/RadioGroup';

export const NumberInput = styled(Input)`
  width: 100%;
  transition: width 0.2s;

  @media (min-width: 576px) {
    max-width: 110px;
    transition: width 0.2s;
  }
`;

export const SubmitButton = styled.button`
  width: 100%;
  padding: 24px;
  text-transform: uppercase;
  background-color: #fc8e1f;
  border: none;
  border-radius: 4px;
  outline: none;
  color: #fff;
  font-weight: 600;
  letter-spacing: -0.5px;
  margin: 0 0 24px 0;

  @media (min-width: 576px) {
    width: auto;
  }

  &:hover {
    cursor: pointer;
    opacity: 0.8;
  }
`;

export const ButtonWrapper = styled.div`
  width: 100%;
  text-align: center;
`;

export const Text = styled.p`
  display: flex;
  align-items: center;
  padding: 8px 0;
  font-size: 14px;
  color: #9e9e9e;
  font-weight: 600;
  letter-spacing: -0.5px;

  @media (min-width: 576px) {
    padding: 0 12px;
  }
`;

export const DateInput = styled(Input)`
  min-width: 200px;
  width: 200px;
`;

export const PaymentInput = styled(NumberInput)`
  width: 94%;
  @media (min-width: 576px) {
    width: 100%;
  }
`;

export const TimeInput = styled(Input)`
  min-width: 140px;
  width: 140px;
`;

export const DurationInput = styled(Input)`
  min-width: 100px;
  width: 100px;
`;

export const PaymentRadioGroup = styled(RadioGroup)`
  width: auto;
  padding: 12px 0 0;

  @media (min-width: 576px) {
    padding: 0 12px 0 0;
  }
`;

export const PaymentMainWrapper = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 576px) {
    flex-direction: row;
  }
`;

export const PaymentInputWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 12px 0 0;

  @media (min-width: 576px) {
    padding: 0;
  }
`;

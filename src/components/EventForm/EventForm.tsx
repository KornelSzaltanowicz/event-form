import * as React from 'react';

// components and external types
import { Card, FormField, Input, Select, ISelectOption, SuccessBar, TextArea } from '../';

//styles
import {
  ButtonWrapper,
  DateInput,
  DurationInput,
  NumberInput,
  SubmitButton,
  Text,
  TimeInput,
  PaymentRadioGroup,
  PaymentMainWrapper,
  PaymentInputWrapper,
  PaymentInput,
} from './EventFormStyled';

//internal types
import {
  IEventFormValues,
  initialFormValues,
  IErrorState,
  requiredField,
  paymentOptions,
  // dayTimeOptions,
} from './types';

// others
import { emailRegexp, mapFormValues } from './utils';
import { PropsFromRedux } from '.';

export interface IEventForm {}

function EventForm(props: IEventForm & PropsFromRedux) {
  const { responsible, categories, fetchResponsibles, fetchCategoriess } = props;
  const [values, setFormValues] = React.useState<IEventFormValues>(initialFormValues);
  const [errors, setErrors] = React.useState<IErrorState | null>(null);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [success, setSuccess] = React.useState<boolean>(false);

  React.useEffect(() => {
    setLoading(true);
    const getResponsiblesAsync = async () => {
      return await fetchResponsibles();
    };
    const getCoordinatorsAsync = async () => {
      return await fetchCategoriess();
    };
    Promise.all([getResponsiblesAsync(), getCoordinatorsAsync()]).then(() => {
      setLoading(false);
    });
  }, [fetchResponsibles, fetchCategoriess]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    let requiredFieldsMapped = requiredField;
    let errorsFromValues = {};
    let hasErrors = false;

    if (values.payment === 'paid_event') {
      requiredFieldsMapped = [...requiredFieldsMapped, 'fee'];
    }

    for (let index = 0; index < requiredFieldsMapped.length; index++) {
      const key = requiredFieldsMapped[index];
      if (values[key] === null || values[key] === '') {
        errorsFromValues = { ...errorsFromValues, [key]: 'Field is required' };
        hasErrors = true;
      }
    }
    setErrors((errors) => ({ ...errors, ...errorsFromValues }));
    if (!hasErrors) {
      const valuesToSubmit = mapFormValues(values);
      console.log(valuesToSubmit);
      setSuccess(true);
    }
  };

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    validateNumber?: boolean
  ) => {
    const { value, name } = event.target;
    if (validateNumber) {
      if (/^(\s*|\d+)$/.test(value)) {
        setErrors((errors) => ({ ...errors, [name]: '' }));
        setFormValues((values) => ({
          ...values,
          [name]: value,
        }));
      }
    } else {
      setErrors((errors) => ({ ...errors, [name]: '' }));
      setFormValues((values) => ({
        ...values,
        [name]: value,
      }));
    }
  };

  const validateEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (emailRegexp.test(event.target.value)) {
      setErrors((errors) => ({ ...errors, email: '' }));
      setFormValues((values) => ({
        ...values,
        email: event.target.value,
      }));
    } else {
      setErrors((errors) => ({ ...errors, email: 'Invalid email format' }));
    }
  };

  const handleRadioChange = (event: React.ChangeEvent<HTMLInputElement>, name: string) => {
    setErrors((errors) => ({ ...errors, [name]: '' }));
    setFormValues((values) => ({ ...values, [name]: event.target.value }));
  };

  const handleSelectChange = (option: ISelectOption, name: string) => {
    setErrors((errors) => ({ ...errors, [name]: '' }));
    setFormValues((values) => ({ ...values, [name]: option }));
  };

  if (success) {
    return <SuccessBar />;
  }

  return (
    <form onSubmit={handleSubmit}>
      <Card title="About">
        <FormField label="title" htmlFor="title" hasError={!!errors?.title} required>
          <Input
            placeholder="Make it short and clear"
            name="title"
            id="title"
            type="text"
            value={values.title}
            onChange={handleInputChange}
            hasError={!!errors?.title}
            error={errors?.title}
          />
        </FormField>
        <FormField label="description" htmlFor="description" hasError={!!errors?.description} required>
          <TextArea
            placeholder="Write about your event, be creative"
            rows={8}
            value={values.description}
            name="description"
            id="description"
            characterLimit={140}
            onChange={handleInputChange}
            hasError={!!errors?.description}
            error={errors?.description}
            additionalText="Max length 140 characters"
          />
        </FormField>
        <FormField label="category">
          <Select
            options={categories as ISelectOption[]}
            selected={values.category}
            onElementClick={(category: ISelectOption) => handleSelectChange(category, 'category')}
            hasError={!!errors?.category}
            error={errors?.category}
            placeholder="Select category"
            loading={loading}
            additionalText="Some non readable text"
          />
        </FormField>
        <FormField label="payment" centered>
          <PaymentMainWrapper>
            <PaymentRadioGroup
              name="payment"
              options={paymentOptions}
              onChange={(e) => handleRadioChange(e, 'payment')}
              selected={values.payment}
            />
            {values.payment === 'paid_event' && (
              <PaymentInputWrapper>
                <PaymentInput
                  type="tel"
                  name="fee"
                  id="fee"
                  value={values.fee}
                  hasError={!!errors?.fee}
                  error={errors?.fee}
                  onChange={(e) => handleInputChange(e, true)}
                />
                <Text>$</Text>
              </PaymentInputWrapper>
            )}
          </PaymentMainWrapper>
        </FormField>
        <FormField label="reward" inline>
          <NumberInput
            type="tel"
            name="reward"
            id="reward"
            value={values.reward}
            onChange={(e) => handleInputChange(e, true)}
          />
          <Text>reward points for attendance</Text>
        </FormField>
      </Card>
      <Card title="Coordinator">
        <FormField label="responsible" htmlFor="responsible" required>
          <Select
            options={responsible as ISelectOption[]}
            selected={values.coordinator}
            onElementClick={(coordinator: ISelectOption) => handleSelectChange(coordinator, 'coordinator')}
            hasError={!!errors?.coordinator}
            error={errors?.coordinator}
            placeholder="Select responsible person"
            loading={loading}
          />
        </FormField>
        <FormField label="email">
          <Input
            placeholder="Email"
            name="email"
            id="email"
            type="email"
            value={values.email}
            onChange={handleInputChange}
            onBlur={validateEmail}
            hasError={!!errors?.email}
            error={errors?.email}
          />
        </FormField>
      </Card>
      <Card title="When">
        <FormField label="starts on" required>
          <DateInput
            placeholder="dd/mm/yyyy"
            name="date"
            id="date"
            type="date"
            value={values.date}
            onChange={handleInputChange}
            hasError={!!errors?.date}
            error={errors?.date}
          />
          <Text>at</Text>
          <TimeInput name="time" id="time" type="time" value={values.time} onChange={handleInputChange} />
          {/* Daytime options are hidden because time input ^ depends on browser locales. 
         It's requires more work/time to get it working properly */}

          {/* <RadioGroup
            name="daytime"
            options={dayTimeOptions}
            selected={values.daytime}
            onChange={(e) => handleRadioChange(e, 'daytime')}
          /> */}
        </FormField>
        <FormField label="duration">
          <DurationInput
            placeholder="Number"
            name="duration"
            id="duration"
            // type tel displays numeric keyboard on mobile phones
            type="tel"
            value={values.duration}
            onChange={(e) => handleInputChange(e, true)}
          />
          <Text>{values.duration > 1 ? 'hours' : 'hour'}</Text>
        </FormField>
      </Card>
      <ButtonWrapper>
        <SubmitButton type="submit">publish event</SubmitButton>
      </ButtonWrapper>
    </form>
  );
}

export default EventForm;

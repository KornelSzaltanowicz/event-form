import { connect, ConnectedProps } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from '../../core/redux/rootReducer';
import { fetchCategoriess } from '../../core/redux/category/actions';
import { fetchResponsibles } from '../../core/redux/responsible/actions';
import EventForm from './EventForm';

const mapStateToprops = (state: RootState) => ({
  categories: state.categories.data,
  responsible: state.responsibles.data,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchCategoriess,
      fetchResponsibles,
    },
    dispatch
  );

const connector = connect(mapStateToprops, mapDispatchToProps);

export type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(EventForm);

import * as React from 'react';
import { ContainerStyled } from './Container.styled';

export interface IContainer extends React.HTMLAttributes<HTMLDivElement> {}

function Container(props: IContainer) {
  return <ContainerStyled {...props}>{props.children}</ContainerStyled>;
}

export default Container;

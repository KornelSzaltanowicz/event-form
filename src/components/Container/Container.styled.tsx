import styled from 'styled-components';

export const ContainerStyled = styled.div`
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  transition: all 0.2s;

  @media (min-width: 576px) and (max-width: 767.98px) {
    max-width: 540px;
  }

  @media (min-width: 768px) and (max-width: 991.98px) {
    max-width: 720px;
  }

  @media (min-width: 992px) and (max-width: 1199.98px) {
    max-width: 960px;
  }

  @media (min-width: 1200px) {
    max-width: 1140px;
  }
`;

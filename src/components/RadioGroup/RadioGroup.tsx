import * as React from 'react';
import { OptionWrapper, RadioInput, RadioLabel, RadioWrapper } from './RadioGroup.styled';

export interface IRadioOption {
  label: string;
  value: string;
}

export interface IRadioGroup extends React.HTMLAttributes<HTMLDivElement> {
  options: IRadioOption[];
  selected: string;
  name: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

function RadioGroup(props: IRadioGroup) {
  const { name, options, onChange, selected, children } = props;
  return (
    <RadioWrapper {...props}>
      {options.map(({ value, label }) => (
        <OptionWrapper key={value}>
          <RadioInput
            type="radio"
            id={value}
            value={value}
            name={name}
            onChange={onChange}
            checked={selected === value}
          />
          <RadioLabel htmlFor={value}>{label}</RadioLabel>
        </OptionWrapper>
      ))}
      {children}
    </RadioWrapper>
  );
}

export default RadioGroup;

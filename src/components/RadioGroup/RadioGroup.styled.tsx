import styled from 'styled-components';

export const RadioWrapper = styled.div`
  width: 100%;
  display: flex;
`;

export const RadioInput = styled.input`
  margin: 0 8px 0 0;
`;

export const OptionWrapper = styled.div`
  display: flex;
  align-items: center;
  &:last-child {
    padding-left: 12px;
  }
`;

export const RadioLabel = styled.label`
  font-size: 14px;
  color: #9e9e9e;
  font-weight: 600;
  letter-spacing: -0.5px;

  &:hover {
    cursor: pointer;
  }
`;

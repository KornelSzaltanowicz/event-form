import styled from 'styled-components';

export const ErrorBadgeStyled = styled.span`
  position: absolute;
  padding: 4px 12px;
  background: #e57373;
  color: #fff;
  font-size: 12px;
  width: 100%;
  min-height: 12px;
  white-space: nowrap;
  left: 0;
  top: 100%;
  border-radius: 0;
  box-sizing: border-box;
  z-index: 1;

  @media (min-width: 992px) {
    width: auto;
    border-radius: 4px;
    left: calc(100% + 12px);
    top: calc(50% - 10px);
  }

  &:after {
    position: absolute;
    content: '';
    left: -6px;
    width: 0;
    height: 0;
    top: calc(50% - 6px);
    border-top: 6px solid transparent;
    border-bottom: 6px solid transparent;
    border-right: 6px solid #e57373;
    display: none;
    @media (min-width: 992px) {
      display: block;
    }
  }
`;

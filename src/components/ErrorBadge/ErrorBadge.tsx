import * as React from 'react';
import { ErrorBadgeStyled } from './ErrorBadge.styled';

export interface IErrorBadge {
  message?: string;
}

function ErrorBadge({ message = 'Field error' }: IErrorBadge) {
  return <ErrorBadgeStyled>{message}</ErrorBadgeStyled>;
}

export default ErrorBadge;

import * as React from 'react';
import ErrorBadge from '../ErrorBadge/ErrorBadge';
import SignatureText from '../SignatureText/SignatureText';
import { TextAreaWrapper, TextAreaStyled, SignatureWrapper } from './TextArea.styled';

export interface ITextArea extends React.ComponentPropsWithoutRef<'textarea'> {
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
  characterLimit?: number;
  additionalText?: string;
  hasError?: boolean;
  error?: string;
}

function TextArea(props: ITextArea) {
  const { error, hasError, ...rest } = props;
  const { characterLimit = 140 } = props;
  const [characters, setCharacters] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const valueLength = event.target.value.trim().length;
    if (valueLength <= characterLimit) {
      setCharacters(valueLength);
      props.onChange(event);
    }
  };

  return (
    <TextAreaWrapper>
      <TextAreaStyled {...rest} onChange={handleChange} hasError={hasError} />
      {hasError && <ErrorBadge message={error} />}
      {!hasError && (
        <SignatureWrapper>
          <SignatureText>{props.additionalText}</SignatureText>
          <SignatureText>
            {characters}/{characterLimit}
          </SignatureText>
        </SignatureWrapper>
      )}
    </TextAreaWrapper>
  );
}

export default TextArea;

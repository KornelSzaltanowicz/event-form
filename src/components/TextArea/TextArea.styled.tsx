import styled from 'styled-components';

export const TextAreaStyled = styled.textarea<{ hasError?: boolean }>`
  width: 100%;
  height: 100%;
  padding: 10px;
  outline: none;
  resize: none;
  color: #9e9e9e;
  box-sizing: border-box;
  border: 1px solid ${({ hasError }) => (hasError ? '#ef5350' : '#e0e0e0')};

  &::placeholder {
    color: #9e9e9e;
    opacity: 1;
  }
`;

export const TextAreaWrapper = styled.div`
  position: relative;
  width: 100%;
`;
export const SignatureText = styled.p<{ hasError?: boolean }>`
  display: inline-block;
  font-size: 12px;
  color: ${({ hasError }) => (hasError ? '#ef5350' : '#9e9e9e')};
`;

export const SignatureWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 12px;
`;

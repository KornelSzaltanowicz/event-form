import * as React from 'react';
import { SpinnerElement, SpinnerWrapper } from './Spinner.styled';

export interface ISpinner {}

function Spinner(props: ISpinner) {
  return (
    <SpinnerWrapper {...props}>
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
      <SpinnerElement />
    </SpinnerWrapper>
  );
}

export default Spinner;

import styled from 'styled-components';
import Container from './../Container/Container';

export const HeaderStyled = styled.header`
  display: flex;
  flex-direction: column;
  height: 100px;
  background-color: #35588e;
`;

export const HeaderBar = styled.div`
  width: 100%;
  height: 20px;
  background-color: #1b3a66;
`;

export const HeaderContainer = styled(Container)`
  display: flex;
  flex-grow: 1;
  align-items: center;
`;

export const HeaderTitle = styled.h1`
  font-size: 24px;
  color: #ffffff;
  padding: 0px 30px;
`;

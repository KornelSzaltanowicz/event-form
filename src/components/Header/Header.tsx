import * as React from 'react';
import { HeaderStyled, HeaderBar, HeaderContainer, HeaderTitle } from './Header.styled';

export interface IHeader {
  title: string;
}

function Header(props: IHeader) {
  return (
    <HeaderStyled>
      <HeaderBar />
      <HeaderContainer>
        <HeaderTitle>{props.title}</HeaderTitle>
      </HeaderContainer>
    </HeaderStyled>
  );
}

export default Header;

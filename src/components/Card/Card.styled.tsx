import styled from 'styled-components';

export const CardStyled = styled.div`
  margin: 24px 0;
  padding: 24px 30px;
  background: #ffffff;
  box-shadow: 2px 2px 12px 0px rgba(194, 194, 194, 0.6);
  border: 1px solid #e0e0e0;
`;

export const CardTitle = styled.p`
  font-size: 20px;
  font-weight: 600;
  color: #35588e;
  padding-bottom: 10px;
  margin-bottom: 10px;
  border-bottom: 1px solid #e0e0e0;
`;

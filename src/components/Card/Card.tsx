import * as React from 'react';
import { CardStyled, CardTitle } from './Card.styled';

export interface ICard {
  title: string;
  children: React.ReactNode;
}

function Card({ title, children, ...rest }: ICard) {
  return (
    <CardStyled {...rest}>
      <CardTitle>{title}</CardTitle>
      {children}
    </CardStyled>
  );
}

export default Card;

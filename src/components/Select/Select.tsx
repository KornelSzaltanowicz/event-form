import * as React from 'react';
import ErrorBadge from '../ErrorBadge/ErrorBadge';
import SignatureText from '../SignatureText/SignatureText';
import { SelectList, SelectedOption, OptionStyled, SelectWrapper, Dash, SelectSpinner } from './Select.styled';

export interface ISelectOption {
  label: string;
  value: string | number;
}

export interface ISelect extends React.HTMLAttributes<HTMLDivElement> {
  placeholder: string;
  selected: ISelectOption | null;
  onElementClick: (option: ISelectOption) => void;
  hasError: boolean;
  options?: ISelectOption[] | null;
  error?: string;
  loading?: boolean;
  additionalText?: string;
}

function Select(props: ISelect) {
  const { options, selected, placeholder, onElementClick, error, hasError, additionalText, loading = false } = props;
  const selectRef = React.useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = React.useState(false);

  React.useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (selectRef.current) {
        if (isOpen && !selectRef.current.contains(event.target as Node)) {
          setIsOpen(false);
        }
      }
    };

    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isOpen]);

  const handleOptionClick = (option: ISelectOption) => {
    setIsOpen(false);
    onElementClick(option);
  };

  return (
    <SelectWrapper ref={selectRef}>
      <SelectedOption onClick={() => setIsOpen(!isOpen)} hasError={hasError}>
        {(selected && selected.label) || placeholder}
        {loading ? <SelectSpinner /> : <Dash isOpen={isOpen} />}
      </SelectedOption>
      {additionalText && <SignatureText>{additionalText}</SignatureText>}
      <SelectList isOpen={isOpen}>
        {options &&
          options.map((option: ISelectOption) => (
            <OptionStyled key={option.value} {...option} onClick={() => handleOptionClick(option)}>
              {option.label}
            </OptionStyled>
          ))}
      </SelectList>
      {hasError && <ErrorBadge message={error} />}
    </SelectWrapper>
  );
}

export default Select;

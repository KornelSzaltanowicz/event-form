import styled from 'styled-components';
import Spinner from '../Spinner/Spinner';

export const SelectWrapper = styled.div`
  position: relative;
  width: 100%;
`;

export const SelectList = styled.ul<{ isOpen: boolean }>`
  list-style-type: none;
  position: absolute;
  top: 36px;
  width: 100%;
  border: 1px solid #e0e0e0;
  border-top: none;
  box-sizing: border-box;
  z-index: 2;

  display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
`;

export const OptionStyled = styled.li`
  background: #fff;
  transition: all 0.2s;
  padding: 10px;
  color: #9e9e9e;
  font-size: 14px;

  &:hover {
    cursor: pointer;
    color: #bdbdbd;
    transition: all 0.2s;
  }

  &:last-child {
    border-botttom: none;
  }
`;

export const SelectedOption = styled.div<{ hasError?: boolean }>`
  position: relative;
  width: 100%;
  font-size: 14px;
  padding: 10px;
  outline: none;
  background-color: #fff;
  color: #9e9e9e;
  text-align: left;
  box-sizing: border-box;
  border: 1px solid ${({ hasError }) => (hasError ? '#ef5350' : '#e0e0e0')};
`;

export const Dash = styled.span<{ isOpen: boolean }>`
  position: absolute;
  top: calc(50% - 4px);
  right: 12px;
  width: 0;
  height: 0;
  border-left: ${({ isOpen }) => (!isOpen ? '6px  solid transparent' : '6px  solid transparent')};
  border-right: ${({ isOpen }) => (!isOpen ? '6px   solid transparent' : '6px   solid transparent')};
  border-bottom: ${({ isOpen }) => (!isOpen ? 'none' : '6px   solid #9e9e9e')};
  border-top: ${({ isOpen }) => (!isOpen ? '6px   solid #9e9e9e' : 'none')};
`;

export const SelectSpinner = styled(Spinner)`
  position: absolute;
  right: 16px;
  top: 6px;
`;

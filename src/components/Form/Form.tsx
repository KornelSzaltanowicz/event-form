import * as React from 'react';
import { FormStyled } from './Form.styled';

export interface IForm extends React.ComponentPropsWithoutRef<'form'> {}

function Form(props: IForm) {
  return <FormStyled {...props} />;
}

export default Form;

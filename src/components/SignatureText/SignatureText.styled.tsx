import styled from 'styled-components';

export const SignatureTextStyled = styled.span<{ hasError?: boolean }>`
  display: inline-block;
  font-size: 12px;
  color: ${({ hasError }) => (hasError ? '#ef5350' : '#9e9e9e')};
`;

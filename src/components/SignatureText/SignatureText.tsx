import * as React from 'react';
import { SignatureTextStyled } from './SignatureText.styled';

export interface ISignatureText extends React.HTMLAttributes<HTMLSpanElement> {}

function SignatureText(props: ISignatureText) {
  if (!props.children) return null;
  return <SignatureTextStyled>{props.children}</SignatureTextStyled>;
}

export default SignatureText;

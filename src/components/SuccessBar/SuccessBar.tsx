import * as React from 'react';
import { SuccessBarStyled, SuccessDescription, SuccessTitle } from './SuccessBar.styled';

export interface ISuccessBar {
  title?: string;
  description?: string;
}

function SuccessBar({ title = 'Success', description = 'Event has been created' }: ISuccessBar) {
  return (
    <SuccessBarStyled>
      <SuccessTitle>{title}</SuccessTitle>
      <SuccessDescription>{description}</SuccessDescription>
    </SuccessBarStyled>
  );
}

export default SuccessBar;

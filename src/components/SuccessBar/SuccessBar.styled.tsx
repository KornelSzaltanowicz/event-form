import styled from 'styled-components';

export const SuccessBarStyled = styled.div`
  margin: 24px 0;
  width: 100%;
  padding: 24px 32px;
  background: #effcf2;
  box-shadow: 2px 2px 12px 0px rgba(194, 194, 194, 0.6);
`;

export const SuccessTitle = styled.h3`
  font-size: 20px;
  font-weight: 600;
  color: #a5d6a7;
`;

export const SuccessDescription = styled.p`
  font-size: 14px;
  font-weight: 600;
  color: #9e9e9e;
  padding-top: 24px;
`;
